package com.wky.common.service.impl;

import com.wky.common.dao.ILoginDao;
import com.wky.common.service.ILoginService;
import com.wky.stusys.entity.Student;
import com.wky.stusys.entity.Teacher;

public class LoginServiceImpl implements ILoginService {
	private ILoginDao loginDao;

	public void setLoginDao(ILoginDao loginDao) {
		this.loginDao = loginDao;
	}

	public Student validateStudent(String userName, String password) {
		return loginDao.validateStudent(userName, password);
	}

	public Teacher validateTeacher(String userName, String password) {
		return loginDao.validateTeacher(userName, password);
	}
}
