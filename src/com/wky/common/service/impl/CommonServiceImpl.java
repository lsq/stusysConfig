package com.wky.common.service.impl;

import com.wky.common.dao.ICommonDao;
import com.wky.common.service.ICommonService;

public class CommonServiceImpl implements ICommonService {

	private ICommonDao commonDao;

	public void setCommonDao(ICommonDao commonDao) {
		this.commonDao = commonDao;
	}

	public ICommonDao getCommonDao() {
		return commonDao;
	}
	
}
