package com.wky.common.service;

import com.wky.stusys.entity.Student;
import com.wky.stusys.entity.Teacher;


public interface ILoginService {

	Student validateStudent(String userName, String password);
	Teacher validateTeacher(String userName, String password);
}