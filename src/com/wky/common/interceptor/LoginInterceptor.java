package com.wky.common.interceptor;

import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

public class LoginInterceptor extends AbstractInterceptor implements SessionAware{
	
	private static final long serialVersionUID = -4118217361451604641L;
	
	protected Map<String, Object> session;
	
	public String intercept(ActionInvocation invocation) throws Exception {
		// 取得请求相关的ActionContext实例
		session = invocation.getInvocationContext().getSession();
		Object user = session.get("currentUser");
		String userName = (String) session.get("adminUserName");
		String password = (String) session.get("adminPassword");
		// 如果没有登陆，都返回重新登陆
		if (user != null || (userName.equals("admin") && password.equals("1qaz2wsx"))) {
			return invocation.invoke();
		}

		return "error";
	}

	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

}
