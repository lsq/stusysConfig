package com.wky.common.dic.event;

public interface IDictionaryChangeItem {

    public Object getDictionaryType();

    public Object getDictionaryChangeParam();

    public Object getDictionaryData();
}
