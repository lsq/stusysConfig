﻿package com.wky.common.dic.event;

import org.springframework.context.ApplicationEvent;

public class DictionaryChangeEvent extends ApplicationEvent {

    private static final long serialVersionUID = 4822796187799921634L;

    public DictionaryChangeEvent(IDictionaryChangeItem item) {
        super(item);
    }

    public IDictionaryChangeItem getDictionaryChangeItem() {
        return (IDictionaryChangeItem) getSource();
    }

}

