package com.wky.common.dic.event;

public class SimpleDictionaryChangeItem implements IDictionaryChangeItem {

    private final Object dictionaryType;

    private final Object dictionaryChangeParam;

    private Object dictionaryData;

    public SimpleDictionaryChangeItem(Object dictionaryType, Object dictionaryChangeParam) {
        this.dictionaryType = dictionaryType;
        this.dictionaryChangeParam = dictionaryChangeParam;
    }

    public SimpleDictionaryChangeItem(Object dictionaryType, Object dictionaryChangeParam, Object dictionaryData) {
        this.dictionaryType = dictionaryType;
        this.dictionaryChangeParam = dictionaryChangeParam;
        this.dictionaryData = dictionaryData;
    }

    public Object getDictionaryChangeParam() {
        return this.dictionaryChangeParam;
    }

    public Object getDictionaryType() {
        return this.dictionaryType;
    }

    public Object getDictionaryData() {
        return dictionaryData;
    }

}
