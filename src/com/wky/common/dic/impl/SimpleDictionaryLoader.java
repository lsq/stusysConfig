package com.wky.common.dic.impl;

import java.util.HashMap;
import java.util.Map;

import com.wky.common.dic.service.DictionaryLoader;

public class SimpleDictionaryLoader extends DictionaryLoader {

    /**  字典内容   */
	private static Map<String, Object> sysDictionary = new HashMap<String, Object>();

    /**    */
    public SimpleDictionaryLoader() {
        super.setMe(this);
    }

	public Map<String, Object> getAllDictionary() {
        return sysDictionary;
    }

	public void refreshData(String identifier, Map<?,?> dictionaryData) {
        sysDictionary.put(identifier, dictionaryData);
    }

    /**
     * @param identifier 字典标识
     * @param dictionaryData 字典数据
     */
	public static void registerData(String identifier, Object dictionaryData) {
        sysDictionary.put(identifier, dictionaryData);
    }

    /** 字典identifier是否已经存在
     * @param identifier 字典标识
     * @return true 已存在 false 未存在
     */
    public static boolean isDictionaryExist(String identifier) {
        return (sysDictionary.containsKey(identifier));
    }

}
