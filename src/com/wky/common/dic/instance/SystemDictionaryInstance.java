package com.wky.common.dic.instance;

import java.util.Map;

import org.springframework.beans.factory.InitializingBean;

import com.wky.common.dic.service.DictionaryLoader;
import com.wky.common.dic.utils.DictionaryUtils;

public class SystemDictionaryInstance implements InitializingBean {

	/** 字典标识 **/
	private String identifier;
	/** 字典数据 **/
	private Map<String, Object> data;

	public void afterPropertiesSet() throws Exception {
		if (DictionaryLoader.getInstance().getAllDictionary().containsKey(
				identifier)) {
			throw new Exception("重复的系统字典标识: " + identifier);
		} else {
			DictionaryLoader.getInstance().refreshData(identifier,
					DictionaryUtils.MapToDictionaryItemMap(data));
		}
	}

	public Map<String, Object> getData() {
		return data;
	}

	public void setData(Map<String, Object> contentMap) {
		this.data = contentMap;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String dictionaryName) {
		this.identifier = dictionaryName;
	}

}
