﻿package com.wky.common.dic.service.impl;

import java.util.Iterator;
import java.util.List;

import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;

import com.wky.common.dic.event.DictionaryChangeEvent;
import com.wky.common.dic.event.IDictionaryChangeItem;
import com.wky.common.dic.service.DictionaryLoader;
import com.wky.common.dic.utils.DictionaryUtils;
import com.wky.common.service.impl.CommonServiceImpl;

public class DictionaryServiceImpl extends CommonServiceImpl implements ApplicationListener<ApplicationEvent>{

    /** 简单字典类型标志，用于当简单字典被修改后发通知用的标志 */
    private static String FLAG_CHANGE_SIMPLE = "simple";

    /** 专业字典类型标志，用于当专业字典被修改后发通知用的标志 */
    private static String FLAG_CHANGE_SPECIALITY = "speciality";

    /**
     * 字典列表,列表中的每一行是一个以逗号分隔的如下格式的字符串:
     * [字典中文名],[字典引用名],[字典对象的类名],[字典对象的key属性名],[字典对象的value属性名]
     */
    private List<String> dictionaryList;

    public void onApplicationEvent(ApplicationEvent event) {
        if (DictionaryChangeEvent.class.isInstance(event)) {
            IDictionaryChangeItem changeItem = ((DictionaryChangeEvent) event).getDictionaryChangeItem();
            if (FLAG_CHANGE_SIMPLE.equals(changeItem.getDictionaryType())) {
                String param = (String) changeItem.getDictionaryChangeParam();
                this.loadDictionaryByString(param);
            } else if (FLAG_CHANGE_SPECIALITY.equals(changeItem.getDictionaryType())) {
            }

        } else if (ContextRefreshedEvent.class.isInstance(event)) {
            try {
                this.loadAllDictionary();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    /**
     * 根据dictionaryList加载所有的字典数据
     */
    private void loadAllDictionary() {
        if (null != this.dictionaryList) {
            for (Iterator<String> iter = this.dictionaryList.iterator(); iter.hasNext();) {
                this.loadDictionaryByString((String) iter.next());
            }
        }
    }

    /**
     * 由字典参数串加载一个字典
     * @param dictString 字典参数串
     */
	private void loadDictionaryByString(String dictString) {
        if (null == dictString) {
            return;
        }
        String[] param = dictString.split(";");
        if (param.length != 5) {
            throw new RuntimeException("字典参数错误![" + dictString + "]");
        }

        try {
            DictionaryLoader.getInstance().refreshData(param[1].trim(),// 字典引用名
                            DictionaryUtils.ListToDictionaryItemMap(
                            		this.getCommonDao().loadAll((Class<?>) Class.forName(param[2].trim())), //字典对象的类名
                                    param[3].trim(),   //字典对象的key属性名
                                    param[4].trim())); //字典对象的value属性名
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("字典参数错误![" + dictString + "]");
        }
    }

    public List<String> getDictionaryList() {
        return dictionaryList;
    }

    public void setDictionaryList(List<String> dictionaryList) {
        this.dictionaryList = dictionaryList;
    }

}

