package com.wky.common.dic.service;

public interface IDictionaryMapValueItem {

    /**
     * 字典Map中数据项的常用名称对象
     * @return 名称对象
     */
    Object getName();

    /**
     * 字典Map中数据项的数据对象
     * @return 数据对象
     */
    Object getData();

}
