package com.wky.common.dic.service;

import java.util.Map;

public abstract class DictionaryLoader {

    /**单实例模式 */
    private static DictionaryLoader me;

    /** 获取对象实例
     * @return Returns the me.
     */
    public static DictionaryLoader getInstance() {
        return me;
    }
    /**
     * @param me The me to set.
     */
    protected void setMe(DictionaryLoader me) {
        DictionaryLoader.me = me;
    }

    /** 获取所有的字典数据
     * @return 以Map方式存储的字典信息
     */
	public abstract Map<String, Object> getAllDictionary();


    /** 刷新某个字典的内存数据
     * @param identifier 字典标识符
     * @param dictionaryData 字典数据
     */
	public abstract void refreshData(String identifier, Map<?, ?> dictionaryData);
}
