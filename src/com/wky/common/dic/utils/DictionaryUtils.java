package com.wky.common.dic.utils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;

import com.wky.common.dic.event.SimpleDictionaryChangeItem;
import com.wky.common.dic.service.DictionaryLoader;
import com.wky.common.dic.service.IDictionaryMapValueItem;

public final class DictionaryUtils {
	/**
	 * 将map转换为value为IDictionaryMapValueItem接口对象的map
	 * 
	 * 转换方法为新map的key与输入的map的key相同，使用Iterator访问的顺序也相同。
	 * 新map的value为实现IDictionaryMapValueItem接口的对象
	 * ，该对象中的valueName和valueData都是输入的map的value
	 * 
	 * @param map
	 *            要转换的Map
	 * @return 转换后的结果Map
	 */
	public static Map<String,Object> MapToDictionaryItemMap(Map<String,Object> map) {
		Map<String,Object> result = new LinkedHashMap<String ,Object>();
		for (Iterator<Entry<String, Object>> iter = map.entrySet().iterator(); iter.hasNext();) {
			Map.Entry<String, Object> element = (Map.Entry<String, Object>) iter.next();
			result.put(element.getKey(), element.getValue());
		}
		return result;
	}

	/**
	 * 将List中的数据转换为value为IDictionaryMapValueItem接口对象的map
	 * 
	 * 转换方法为新map的key为List中元素对象的keyPropertyName属性的对象(当该对象是Number时，将被转换为String对象)，
	 * 新map使用Iterator访问的顺序与List的相同。 新map的value为实现IDictionaryMapValueItem接口的对象，
	 * 该对象中的valueName为List中元素对象的valueNamePropertyName属性的对象 valueData就是List中的元素对象
	 * 
	 * @param list
	 *            输入的List
	 * @param keyPropertyName
	 *            输入的List中的元素对象中记录key值的属性的名称
	 * @param valueNamePropertyName
	 *            输入的List中的元素对象中记录常用名称值的属性的名称
	 * @return 转换后的结果Map
	 */
	public static Map<Object, ValueItem> ListToDictionaryItemMap(List<?> list,
			String keyPropertyName, String valueNamePropertyName) {
		Map<Object, ValueItem> result = new LinkedHashMap<Object, ValueItem>();

		for (Iterator<?> iter = list.iterator(); iter.hasNext();) {
			Object element = iter.next();
			BeanWrapper beanObject = new BeanWrapperImpl(element);
			Object keyObject = beanObject.getPropertyValue(keyPropertyName);
			if (Number.class.isInstance(keyObject)) {
				keyObject = keyObject.toString();
			}
			result.put(keyObject, new DictionaryUtils.ValueItem(beanObject
					.getPropertyValue(valueNamePropertyName), element));

		}
		return result;
	}

	/**
	 * 替换Map中的字典数据
	 * 
	 * @param data
	 *            新字典数据
	 * @param dicName
	 *            字典名称
	 * @param keyPropertyName
	 * @param valueNamePropertyName
	 */
	public static void dataToDictionaryItemMap(Map<Object, ValueItem> dicData, Object data,
			String keyPropertyName, String valueNamePropertyName) {
		BeanWrapper beanObject = new BeanWrapperImpl(data);
		Object keyObject = beanObject.getPropertyValue(keyPropertyName);
		if (Number.class.isInstance(keyObject)) {
			keyObject = keyObject.toString();
		}
		dicData.put(keyObject, new DictionaryUtils.ValueItem(beanObject
				.getPropertyValue(valueNamePropertyName), data));
	}

	/**
	 * 根据字典名称和键值查找字典内容
	 * 
	 * @param dicName
	 *            字典名称
	 * @param keyId
	 *            键值
	 * @return 字典内容
	 */
	@SuppressWarnings("unchecked")
	public static Object getObjectFormMap(String dicName, Object keyId) {
		Map<String,Object> dicData = (Map<String, Object>) DictionaryLoader.getInstance().getAllDictionary()
				.get(dicName);
		if (dicData != null && !dicData.isEmpty()) {
			IDictionaryMapValueItem item = (IDictionaryMapValueItem) getMapData(
					dicData, keyId);
			if (item != null) {
				return item.getData();
			}
		}
		return null;
	}

	private static Object getMapData(Map<String, Object> dicData, Object key) {
		Object result = dicData.get(key);
		if ((null == result) && (null != key) && (Number.class.isInstance(key))) {
			result = dicData.get(key.toString());
		}
		return result;
	}

	/**
	 * 根据字典名称和字典内容查找键值
	 * 
	 * @param dicName
	 *            字典名称
	 * @param value
	 *            内容
	 * @return 键值
	 */
	@SuppressWarnings("unchecked")
	public static Object getKeyFormMap(String dicName, Object value) {
		Map<?, Object> dicData = (Map<?, Object>) DictionaryLoader.getInstance().getAllDictionary()
				.get(dicName);
		if (dicData != null && !dicData.isEmpty()) {
			for (Iterator<?> iter = dicData.entrySet().iterator(); iter.hasNext();) {
				Map.Entry<?, Object> element = (Entry<?, Object>) iter.next();
				if (((ValueItem) element.getValue()).getData().equals(value)) {
					return element.getKey();
				}
			}
		}
		return null;
	}

	/**
	 * 根据参数Map分解SimpleDictionaryChangeItem 列表
	 * 
	 * @param dicParams
	 *            参数Map
	 * @return SimpleDictionaryChangeItem 列表
	 */
	public static List<SimpleDictionaryChangeItem> getDictionaryChangeItems(Map<Object, Object> dicParams) {
		List<SimpleDictionaryChangeItem> sdc = new ArrayList<SimpleDictionaryChangeItem>();
		String dictionaryChangeParam;
		for (Iterator<Entry<Object, Object>> it = dicParams.entrySet().iterator(); it.hasNext();) {
			Entry<Object, Object> entry = (Entry<Object, Object>) it.next();
			dictionaryChangeParam = (String) entry.getValue();
			if (dictionaryChangeParam.indexOf(",") > 0) {
				String[] items = dictionaryChangeParam.split(",");
				for (int i = 0; i < items.length; i++) {
					sdc.add(new SimpleDictionaryChangeItem(entry.getKey(),
							items[i]));
				}
			} else {
				sdc.add(new SimpleDictionaryChangeItem(entry.getKey(),
						dictionaryChangeParam));
			}
		}
		return sdc;
	}

	public static class ValueItem implements IDictionaryMapValueItem {

		/** 项名称 */
		private Object name;

		/** 项数据 */
		private Object data;

		/**
		 * 构造方法
		 * 
		 * @param valueName
		 *            项名称
		 * @param valueData
		 *            项数据
		 */
		public ValueItem(Object name, Object data) {
			this.name = name;
			this.data = data;
		}

		public Object getData() {
			return this.data;
		}

		public Object getName() {
			return this.name;
		}

		public String toString() {
			return "[" + this.name + this.data + "]";
		}
	}
}
