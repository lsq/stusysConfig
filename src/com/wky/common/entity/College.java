package com.wky.common.entity;

import java.io.Serializable;

public class College implements Serializable {

	private static final long serialVersionUID = -6763672041871532009L;
	
	private Long collegeId;
	private String collegeName;
	private Long parentId;
	private Long AncestorId;

	public College() {
	}

	public Long getCollegeId() {
		return collegeId;
	}

	public void setCollegeId(Long collegeId) {
		this.collegeId = collegeId;
	}

	public String getCollegeName() {
		return collegeName;
	}

	public void setCollegeName(String collegeName) {
		this.collegeName = collegeName;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public Long getAncestorId() {
		return AncestorId;
	}

	public void setAncestorId(Long ancestorId) {
		AncestorId = ancestorId;
	}

}
