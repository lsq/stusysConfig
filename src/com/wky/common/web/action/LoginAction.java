package com.wky.common.web.action;

import com.wky.common.service.ILoginService;
import com.wky.stusys.entity.Student;
import com.wky.stusys.entity.Teacher;

public class LoginAction extends BaseAction {

	private static final long serialVersionUID = -304772250240426252L;

	private static final String STUDENT = "1";
	private static final String TEACHER = "2";
	private static final String ADMIN = "3";

	private String userName;
	private String password;
	private String type;

	private ILoginService loginService;

	public void setLoginService(ILoginService loginService) {
		this.loginService = loginService;
	}

	public String login() {

		if (type.equals(STUDENT)) {
			Student student = loginService.validateStudent(userName, password);
			if (student != null) {
				session.put("currentUser", student);
				return "student";
			} else {
				return "loginError";
			}
		} else if (type.equals(TEACHER)) {
			Teacher teacher = loginService.validateTeacher(userName, password);
			if (teacher != null) {
				session.put("currentUser", teacher);
				return "teacher";
			} else {
				return "loginError";
			}
		} else if (type.equals(ADMIN)) {
			if (userName.equals("admin") && password.equals("1qaz2wsx")) {
				session.put("adminUserName", userName);
				session.put("adminPassword", password);
				return "admin";
			} else {
				return "loginError";
			}
		} else {
			return "error";
		}
	}
	
	public String loginOut(){
		session.clear();
		return SUCCESS;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
