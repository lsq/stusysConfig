package com.wky.common.web.action;

import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;

public class BaseAction extends ActionSupport implements SessionAware {
	
	private static final long serialVersionUID = 6721237201147917882L;
	protected Map<String, Object> session;

	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

}
