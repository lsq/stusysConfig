package com.wky.common.dao.impl;

import java.util.List;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.wky.common.dao.ILoginDao;
import com.wky.common.util.Tools;
import com.wky.stusys.entity.Student;
import com.wky.stusys.entity.Teacher;

public class LoginDaoImpl extends HibernateDaoSupport implements ILoginDao {

	@SuppressWarnings("unchecked")
	public Student validateStudent(String userName, String password) {
		StringBuffer hsql = new StringBuffer();
		hsql.append("from ").append(Student.class.getName()).append(" as s where s.userName = ? and s.password = ?");
		List<Student> students =  this.getHibernateTemplate().find(hsql.toString(), new Object[]{userName, password});
		if(Tools.isNotEmptyList(students)){
			return students.get(0);
		} else {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public Teacher validateTeacher(String userName, String password) {
		StringBuffer hsql = new StringBuffer();
		hsql.append("from ").append(Teacher.class.getName()).append(" as t where t.userName = ? and t.password = ?");
		List<Teacher> teachers =  this.getHibernateTemplate().find(hsql.toString(), new Object[]{userName, password});
		if(Tools.isNotEmptyList(teachers)){
			return teachers.get(0);
		} else {
			return null;
		}
	}


}
