﻿package com.wky.common.dao;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import com.wky.common.page.Page;

public interface ICommonDao {

    /**
     * 保存一个新的对象
     * @param entity    需保存的对象
     */
    void insert(final Object entity);

    /**
     * 更新一个对象
     * @param entity    需更新的对象
     */
    void update(final Object entity);

    /**
     * 新增或更新一个对象
     * @param entity    需保存的对象
     */
    void save(final Object entity);

    /**
     * 新增或更新一个对象，然后重新从数据库读取该对象。
     * 主要用于要保存的对象数据里有关联对象，并希望在保存该对象后，对象里的关联对象被加载好的情况。
     * @param entity    需保存的对象
     */
    void saveAndRefresh(final Object entity);

    /**
     * 新增或更新给定的全部对象
     * @param entities  需保存的对象
     */
    void saveAll(final Collection<Object> entities);

    /**
     * 删除一个对象
     * @param entity    需删除的对象
     */
    void delete(final Object entity);

    /**
     * 删除给定的全部持久对象
     * @param entities  要删除的持久对象
     */
    void deleteAll(final Collection<Object> entities);

    /**
 	 * 删除给定的全部持久对象
     * @param entity 要删除的持久对象
     */
    void deleteAll(final Class<Object> entity);

    /**
     * Bulk方式删除对象
     * @param entity 要删除的持久对象
     * @param id 要删除的持久对象的编号
     */
    void deleteByBulk(final Class<Object> entity, Object id);

    /**
     * 批量更新语句
     * @param updateStirng 执行语句
     * @param values 参数值
     */
    void updateByBulk(final String updateStirng, final Object[] values);
    /**
     * 取一个持久对象
     * @param entityClass   持久类
     * @param id            标识
     * @return  持久对象
     */
    Object load(final Class<?> entityClass, final Serializable id);

    /** 获取一个类的所有实例
     * @param entityClass 类型
     * @return 对象列表
     */
    List<?> loadAll(final Class<?> entityClass);

    /**
     * 初始化一个代理对象
     * @param proxy     代理对象
     */
    void initialize(Object proxy);

    /**
     * 执行查询语句,返回结果列表
     * @param queryString 查询语句
     * @return 结果列表
     */
    List<?> executeSql(String queryString);

    /**
     * 执行查询语句,返回结果列表
     * @param queryString 查询语句
     * @param values        参数值
     * @return 结果列表
     */
    List<?> executeSql(String queryString, final Object[] values);

    /**
     * 清理cache，清理方法如下：
     *     flush
     *     clear
     */
    void pureCacheObject();
    /**
     * 获取hibernate的缓存对象
     * @return session
     */
    //Session getHibernateSession();
//////////分页查询
    /**
     * @param queryString   HSQL
     * @param pageNumber    当前页码
     * @return  Page实例
     */
    Page<?> find(final String queryString, final int pageNumber);

    /**
     * @param queryString   HSQL
     * @param pageNumber    当前页码
     * @param pageSize      每页显示的记录数
     * @return  Page实例
     */
    Page<?> find(final String queryString, final int pageNumber, final int pageSize);

    /**
     * @param queryString   HSQL
     * @param value         参数值
     * @param pageNumber    当前页码
     * @return  Page实例
     */
    Page<?> find(final String queryString, final Object value, final int pageNumber);

    /**
     * @param queryString   HSQL
     * @param value         参数值
     * @param pageNumber    当前页码
     * @param pageSize      每页显示的记录数
     * @return  Page实例
     */
    Page<?> find(final String queryString, final Object value,
            final int pageNumber, final int pageSize);

    /**
     * @param queryString   HSQL
     * @param values        参数值
     * @param pageNumber    当前页码
     * @return  Page实例
     */
    Page<?> find(final String queryString, final Object[] values, final int pageNumber);

    /**
     * @param queryString   HSQL
     * @param values        参数值
     * @param pageNumber    当前页码
     * @param pageSize      每页显示的记录数
     * @return  Page实例
     */
    Page<?> find(final String queryString, final Object[] values,
            final int pageNumber, final int pageSize);
/**
     * @param queryString   HSQL
     * @param fieldString   显示字段
     * @param values        参数值
     * @param pageNumber    当前页码
     * @return  Page实例
     */
    Page<?> find(final String queryString, final String fieldString, final Object[] values,
            final int pageNumber);
    /**
     * 建立一个空的Page，并将data放到page的data中。
     * @param data 数据
     * @return page
     */
    Page<?> putDataToPage(List<?> data);
    /**
     * 建立一个空的Page，并将data放到page的data中。
     * @param data
     * @param pageNumber
     * @return
     */
    Page<?> putDataToPage(List<?> data, final int pageNumber);
    /**
     * 获取hibernate的Fetch Size值
     * @return hibernate的Fetch Size值
     */
    int getHibernateFetchSize();
    /**
     * 倒出并清空缓存
     */
    void flushAndClearCache();
    /**
     * 获取hibernate的Batch Size值
     * @return hibernate的Batch Size值
     */
    int getHibernateBatchSize();

}

