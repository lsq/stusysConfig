package com.wky.common.dao;

import com.wky.stusys.entity.Student;
import com.wky.stusys.entity.Teacher;


public interface ILoginDao{
	Student validateStudent(String userName, String password);
	Teacher validateTeacher(String userName, String password);
}
