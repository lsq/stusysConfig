package com.wky.common.query;


public class QueryInfo {
	
	private Integer pageNumber;
	
	private Integer pageSize;
	
	private Boolean notPage;
	
	public Integer getPageNumber() {
        if (pageNumber == null) {
            return new Integer(0);
        }
        return pageNumber;
    }
	
	public void setPageNumber(Integer pageNumber) {
        this.pageNumber = pageNumber;
    }

	public Boolean getNotPage() {
		return notPage;
	}

	public void setNotPage(Boolean notPage) {
		this.notPage = notPage;
	}

	public Integer getPageSize() {
		if (pageSize == null) {
            return new Integer(2);
        }
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
	
}
