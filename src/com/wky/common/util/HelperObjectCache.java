﻿/*
 * FileName:    HelperObjectCache.java
 * Description:
 * Company:     南宁超创信息工程有限公司
 * Copyright:   ChaoChuang (c) 2005
 * History:     2005-4-1 (guig) 1.0 Create
 */
package com.wky.common.util;

import java.util.Map;

/**
 * @author guig
 * @version 1.0 2005-4-1
 */
public final class HelperObjectCache {

    private static Map<String, ?> helperMap = null;

    private static boolean allowHelperOverride = false;

    /**
     * @return Returns the helperMap.
     */
    public static Map<String, ?> getHelperMap() {
        return helperMap;
    }

    /**
     * @param helperMap The helperMap to set.
     */
    public void setHelperMap(Map<String, ?> helperMap) {
        HelperObjectCache.helperMap = helperMap;
    }

    /**
     * @return  是否允许覆盖同名上下文
     */
    public static boolean getAllowHelperOverride() {
        return allowHelperOverride;
    }

    /**
     * @param allowHelperOverride The allowHelperOverride to set.
     */
    public void setAllowHelperOverride(boolean allowHelperOverride) {
        HelperObjectCache.allowHelperOverride = allowHelperOverride;
    }
}

