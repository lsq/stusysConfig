package com.wky.example.web.action;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.wky.common.dic.service.DictionaryLoader;
import com.wky.common.web.action.BaseAction;
import com.wky.example.entity.User;
import com.wky.example.service.IUserService;

public class UserAction extends BaseAction {

	private static final long serialVersionUID = 6101832433194460174L;

	private IUserService userService;
	private List<User> users;
	private Long id;
	private User user = new User();
	private Map<String, Object> dicData = DictionaryLoader.getInstance().getAllDictionary();

	public IUserService getUserService() {
		return userService;
	}

	public void setUserService(IUserService userService) {
		this.userService = userService;
	}

	public String toAddUser() {
		return SUCCESS;
	}

	public String addUser() {
		Long id = userService.addUser(user);
		System.out.println(id);
		return SUCCESS;
	}

	public String deleteUser() {
		User dUser = new User();
		dUser.setId(id);
		userService.deleteUser(dUser);
		return SUCCESS;
	}

	public String toUpdateUser() {
		user = userService.findUser(id);
		return SUCCESS;
	}

	public String updateUser() {
		userService.updateUser(user);
		return SUCCESS;
	}

	public String findUser() {
		return SUCCESS;
	}

	public String findUserList() {
		users = new ArrayList<User>();
		users = userService.findUserList(user);
		return SUCCESS;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Map<String, Object> getDicData() {
		return dicData;
	}

	public void setDicData(Map<String, Object> dicData) {
		this.dicData = dicData;
	}

}
