package com.wky.example.service;

import java.util.List;

import com.wky.example.entity.User;

public interface IUserService {
	public Long addUser(User user);

	public void deleteUser(User user);

	public void updateUser(User user);
	
	public User findUser(Long id);

	public List<User> findUserList(User user);
}
