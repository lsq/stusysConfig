package com.wky.example.service.impl;

import java.util.List;

import com.wky.example.dao.IUserDao;
import com.wky.example.entity.User;
import com.wky.example.service.IUserService;

public class UserServiceImpl implements IUserService {

	private IUserDao userDao;

	public IUserDao getUserDao() {
		return userDao;
	}

	public void setUserDao(IUserDao userDao) {
		this.userDao = userDao;
	}

	public Long addUser(User user) {
		return userDao.addUser(user);
	}

	public void deleteUser(User user) {
		userDao.deleteUser(user);
	}

	public void updateUser(User user) {
		userDao.updateUser(user);
	}

	public List<User> findUserList(User user) {
		return userDao.findUserList(user);
	}

	public User findUser(Long id) {
		return userDao.findUser(id);
	}

}
