package com.wky.stusys.entity;

import java.io.Serializable;
import java.util.Date;

import com.wky.common.util.DateUtil;

public class Teacher implements Serializable {

	private static final long serialVersionUID = 4268434291453187993L;

	private Integer teaId;
	private String teaName;
	private String teaSex;
	private Integer teaAge;
	private Date teaBirthday;
	private String teaCollege;
	private String userName;
	private String password;

	public Teacher() {
	}

	public Integer getTeaId() {
		return this.teaId;
	}

	public void setTeaId(Integer teaId) {
		this.teaId = teaId;
	}

	public String getTeaName() {
		return this.teaName;
	}

	public void setTeaName(String teaName) {
		this.teaName = teaName;
	}

	public String getTeaSex() {
		return this.teaSex;
	}

	public void setTeaSex(String teaSex) {
		this.teaSex = teaSex;
	}

	public Integer getTeaAge() {
		return this.teaAge;
	}

	public void setTeaAge(Integer teaAge) {
		this.teaAge = teaAge;
	}

	public Date getTeaBirthday() {
		return this.teaBirthday;
	}

	public void setTeaBirthday(Date teaBirthday) {
		this.teaBirthday = teaBirthday;
	}

	public String getTeaCollege() {
		return teaCollege;
	}

	public void setTeaCollege(String teaCollege) {
		this.teaCollege = teaCollege;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String formatDate() {
		if (this.teaBirthday != null) {
			return DateUtil.DATE_FORMAT_YMD.format(this.teaBirthday).toString();
		} else {
			return null;
		}
	}
}