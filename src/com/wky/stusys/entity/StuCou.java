package com.wky.stusys.entity;

import java.io.Serializable;

public class StuCou implements Serializable {

	private static final long serialVersionUID = 7956850856254344373L;

	private Integer stuCouId;
	private Integer stuId;
	private Integer couId;
	private Integer teaId;
	private Double score;
	private Course course;
	private Student student;
	private Teacher teacher;

	public StuCou() {
	}

	public Integer getStuCouId() {
		return stuCouId;
	}

	public void setStuCouId(Integer stuCouId) {
		this.stuCouId = stuCouId;
	}

	public Integer getStuId() {
		return stuId;
	}

	public void setStuId(Integer stuId) {
		this.stuId = stuId;
	}

	public Integer getCouId() {
		return couId;
	}

	public void setCouId(Integer couId) {
		this.couId = couId;
	}

	public Integer getTeaId() {
		return teaId;
	}

	public void setTeaId(Integer teaId) {
		this.teaId = teaId;
	}

	public Double getScore() {
		return score;
	}

	public void setScore(Double score) {
		this.score = score;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public Teacher getTeacher() {
		return teacher;
	}

	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}

}
