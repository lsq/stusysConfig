package com.wky.stusys.entity;

import java.io.Serializable;

public class Course implements Serializable {

	private static final long serialVersionUID = -2507911450857931272L;

	private Integer couId;
	private String couName;
	private String couRemark;
	private Double couCredit;

	public Course() {
	}

	public Integer getCouId() {
		return this.couId;
	}

	public void setCouId(Integer couId) {
		this.couId = couId;
	}

	public String getCouName() {
		return this.couName;
	}

	public void setCouName(String couName) {
		this.couName = couName;
	}

	public String getCouRemark() {
		return this.couRemark;
	}

	public void setCouRemark(String couRemark) {
		this.couRemark = couRemark;
	}

	public Double getCouCredit() {
		return this.couCredit;
	}

	public void setCouCredit(Double couCredit) {
		this.couCredit = couCredit;
	}

}