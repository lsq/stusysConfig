package com.wky.stusys.entity;

import java.io.Serializable;
import java.util.Date;

import com.wky.common.util.DateUtil;

public class Student implements Serializable {

	private static final long serialVersionUID = 7916339100014714130L;

	private Integer stuId;
	private String stuName;
	private String stuSex;
	private Integer stuAge;
	private Date stuBirthday;
	private String stuClass;
	private String userName;
	private String password;

	public Student() {
	}

	public Integer getStuId() {
		return this.stuId;
	}

	public void setStuId(Integer stuId) {
		this.stuId = stuId;
	}

	public String getStuName() {
		return this.stuName;
	}

	public void setStuName(String stuName) {
		this.stuName = stuName;
	}

	public String getStuSex() {
		return this.stuSex;
	}

	public void setStuSex(String stuSex) {
		this.stuSex = stuSex;
	}

	public Integer getStuAge() {
		return this.stuAge;
	}

	public void setStuAge(Integer stuAge) {
		this.stuAge = stuAge;
	}

	public Date getStuBirthday() {
		return this.stuBirthday;
	}

	public void setStuBirthday(Date stuBirthday) {
		this.stuBirthday = stuBirthday;
	}

	public String getStuClass() {
		return stuClass;
	}

	public void setStuClass(String stuClass) {
		this.stuClass = stuClass;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public String formatDate(){
		if(this.stuBirthday != null){
			return DateUtil.DATE_FORMAT_YMD.format(this.stuBirthday).toString();
		} else {
			return null;
		}
	}

}