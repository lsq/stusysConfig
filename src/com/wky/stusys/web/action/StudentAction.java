package com.wky.stusys.web.action;

import java.util.Map;

import com.wky.common.dic.service.DictionaryLoader;
import com.wky.common.page.Page;
import com.wky.common.web.action.BaseAction;
import com.wky.stusys.entity.StuCou;
import com.wky.stusys.entity.Student;
import com.wky.stusys.query.StuSysQueryInfo;
import com.wky.stusys.service.IStudentService;

public class StudentAction extends BaseAction {

	private static final long serialVersionUID = 6101832433194460174L;

	private IStudentService studentService;
	private Page<StuCou> stuCous;
	private Student currentUser = new Student();
	private Student student = new Student();
	private StuSysQueryInfo info = new StuSysQueryInfo();
	private Integer stuId;
	private Map<String, Object> dicData = DictionaryLoader.getInstance().getAllDictionary();

	public String toUpdateStudent() {
		student = studentService.findStudent(info);
		return SUCCESS;
	}

	public String updateStudent() {
		studentService.updateStudent(student);
		return SUCCESS;
	}

	public String findStudentScorePage() {
		currentUser = (Student) session.get("currentUser");
		info.setStuId(currentUser.getStuId());
		stuCous = studentService.findStudentScorePage(info);
		return SUCCESS;
	}

	public Student getCurrentUser() {
		return currentUser;
	}

	public void setCurrentUser(Student currentUser) {
		this.currentUser = currentUser;
	}

	public Integer getStuId() {
		return stuId;
	}

	public void setStuId(Integer stuId) {
		this.stuId = stuId;
	}

	public void setStudentService(IStudentService studentService) {
		this.studentService = studentService;
	}

	public void setDicData(Map<String, Object> dicData) {
		this.dicData = dicData;
	}

	public Map<String, Object> getDicData() {
		return dicData;
	}

	public StuSysQueryInfo getInfo() {
		return info;
	}

	public void setInfo(StuSysQueryInfo info) {
		this.info = info;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public Page<StuCou> getStuCous() {
		return stuCous;
	}

	public void setStuCous(Page<StuCou> stuCous) {
		this.stuCous = stuCous;
	}

}
