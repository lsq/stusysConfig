package com.wky.stusys.web.action;

import java.util.Map;

import com.wky.common.dic.service.DictionaryLoader;
import com.wky.common.page.Page;
import com.wky.common.web.action.BaseAction;
import com.wky.stusys.entity.StuCou;
import com.wky.stusys.entity.Teacher;
import com.wky.stusys.query.StuSysQueryInfo;
import com.wky.stusys.service.ITeacherService;

public class TeacherAction extends BaseAction {

	private static final long serialVersionUID = 6101832433194460174L;

	private ITeacherService teacherService;
	private Page<StuCou> stuCous;
	private Teacher currentUser = new Teacher();
	private Teacher teacher = new Teacher();
	private StuCou stuCou = new StuCou();
	private StuSysQueryInfo info = new StuSysQueryInfo();
	private Map<String, Object> dicData = DictionaryLoader.getInstance().getAllDictionary();

	public String toUpdateTeacher() {
		teacher = teacherService.findTeacher(info.getTeaId());
		return SUCCESS;
	}

	public String updateTeacher() {
		teacherService.updateTeacher(teacher);
		return SUCCESS;
	}

	public String findTeacherScorePage() {
		currentUser = (Teacher) session.get("currentUser");
		info.setTeaId(currentUser.getTeaId());
		stuCous = teacherService.findTeacherScorePage(info);
		return SUCCESS;
	}
	
	public String toUpdateStuCou(){
		stuCou =  teacherService.findStuCou(info.getStuCouId());
		return SUCCESS;
	}
	
	public String updateStuCou() {
		teacherService.updateStuCou(stuCou);
		return SUCCESS;
	}

	public Teacher getCurrentUser() {
		return currentUser;
	}

	public void setCurrentUser(Teacher currentUser) {
		this.currentUser = currentUser;
	}

	public Teacher getTeacher() {
		return teacher;
	}

	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}

	public void setTeacherService(ITeacherService teacherService) {
		this.teacherService = teacherService;
	}

	public void setDicData(Map<String, Object> dicData) {
		this.dicData = dicData;
	}

	public Map<String, Object> getDicData() {
		return dicData;
	}

	public StuSysQueryInfo getInfo() {
		return info;
	}

	public void setInfo(StuSysQueryInfo info) {
		this.info = info;
	}

	public Page<StuCou> getStuCous() {
		return stuCous;
	}

	public void setStuCous(Page<StuCou> stuCous) {
		this.stuCous = stuCous;
	}

	public StuCou getStuCou() {
		return stuCou;
	}

	public void setStuCou(StuCou stuCou) {
		this.stuCou = stuCou;
	}

}
