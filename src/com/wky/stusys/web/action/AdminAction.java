package com.wky.stusys.web.action;

import java.util.Map;

import com.wky.common.dic.service.DictionaryLoader;
import com.wky.common.page.Page;
import com.wky.common.web.action.BaseAction;
import com.wky.stusys.entity.Course;
import com.wky.stusys.entity.StuCou;
import com.wky.stusys.entity.Student;
import com.wky.stusys.entity.Teacher;
import com.wky.stusys.query.StuSysQueryInfo;
import com.wky.stusys.service.IAdminService;

public class AdminAction extends BaseAction {

	private static final long serialVersionUID = 6101832433194460174L;

	private IAdminService adminService;
	private Map<String, Object> dicData = DictionaryLoader.getInstance().getAllDictionary();
	private Student student = new Student();
	private Teacher teacher = new Teacher();
	private Course course = new Course();
	private StuCou stuCou = new StuCou();
	private StuSysQueryInfo info = new StuSysQueryInfo();
	private Page<Student> students;
	private Page<Teacher> teachers;
	private Page<Course> courses;
	private Page<StuCou> stuCous;
	
	private String adminUserName;

	public void setAdminService(IAdminService adminService) {
		this.adminService = adminService;
	}
	
	public String navigation(){
		adminUserName = (String) session.get("adminUserName");
		return SUCCESS;
	}

	public String findStudentPage() {
		students = adminService.findStudentPage(info);
		adminUserName = (String) session.get("adminUserName");
		return SUCCESS;
	}

	public String toAddStudent() {
		return SUCCESS;
	}

	public String addStudent() {
		adminService.addStudent(student);
		return SUCCESS;
	}

	public String toUpdateStudent() {
		student = adminService.toUpdateStudent(info.getStuId());
		return SUCCESS;
	}

	public String updateStudent() {
		adminService.updateStudent(student);
		return SUCCESS;
	}

	public String findTeacherPage() {
		teachers = adminService.findTeacherPage(info);
		adminUserName = (String) session.get("adminUserName");
		return SUCCESS;
	}

	public String toAddTeacher() {
		return SUCCESS;
	}

	public String addTeacher() {
		adminService.addTeacher(teacher);
		return SUCCESS;
	}

	public String toUpdateTeacher() {
		teacher = adminService.toUpdateTeacher(info.getTeaId());
		return SUCCESS;
	}

	public String updateTeacher() {
		adminService.updateTeacher(teacher);
		return SUCCESS;
	}

	public String findCoursePage() {
		courses = adminService.findCoursePage(info);
		adminUserName = (String) session.get("adminUserName");
		return SUCCESS;
	}

	public String toAddCourse() {
		return SUCCESS;
	}

	public String addCourse() {
		adminService.addCourse(course);
		return SUCCESS;
	}

	public String toUpdateCourse() {
		course = adminService.toUpdateCourse(info.getCouId());
		return SUCCESS;
	}

	public String updateCourse() {
		adminService.updateCourse(course);
		return SUCCESS;
	}

	public String findStuCouPage() {
		stuCous = adminService.findStuCouPage(info);
		adminUserName = (String) session.get("adminUserName");
		return SUCCESS;
	}

	public String toAddStuCou() {
		info.setNotPage(true);
		students = adminService.findStudentPage(info);
		teachers = adminService.findTeacherPage(info);
		courses = adminService.findCoursePage(info);
		return SUCCESS;
	}

	public String addStuCou() {
		adminService.addStuCou(stuCou);
		return SUCCESS;
	}

	public String deleteStuCou() {
		StuCou delStuCou = new StuCou();
		delStuCou.setStuCouId(info.getStuCouId());
		adminService.deleteStuCou(delStuCou);
		return SUCCESS;
	}

	public String toUpdateStuCou() {
		stuCou = adminService.toUpdateStuCou(info.getStuCouId());
		students = adminService.findStudentPage(info);
		teachers = adminService.findTeacherPage(info);
		courses = adminService.findCoursePage(info);
		return SUCCESS;
	}

	public String updateStuCou() {
		adminService.updateStuCou(stuCou);
		return SUCCESS;
	}

	public Map<String, Object> getDicData() {
		return dicData;
	}

	public void setDicData(Map<String, Object> dicData) {
		this.dicData = dicData;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public Teacher getTeacher() {
		return teacher;
	}

	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public StuCou getStuCou() {
		return stuCou;
	}

	public void setStuCou(StuCou stuCou) {
		this.stuCou = stuCou;
	}

	public StuSysQueryInfo getInfo() {
		return info;
	}

	public void setInfo(StuSysQueryInfo info) {
		this.info = info;
	}

	public Page<Student> getStudents() {
		return students;
	}

	public void setStudents(Page<Student> students) {
		this.students = students;
	}

	public Page<Teacher> getTeachers() {
		return teachers;
	}

	public void setTeachers(Page<Teacher> teachers) {
		this.teachers = teachers;
	}

	public Page<Course> getCourses() {
		return courses;
	}

	public void setCourses(Page<Course> courses) {
		this.courses = courses;
	}

	public Page<StuCou> getStuCous() {
		return stuCous;
	}

	public void setStuCous(Page<StuCou> stuCous) {
		this.stuCous = stuCous;
	}

	public String getAdminUserName() {
		return adminUserName;
	}

	public void setAdminUserName(String adminUserName) {
		this.adminUserName = adminUserName;
	}

}
