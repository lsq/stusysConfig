package com.wky.stusys.service;

import com.wky.common.page.Page;
import com.wky.stusys.entity.Course;
import com.wky.stusys.entity.StuCou;
import com.wky.stusys.entity.Student;
import com.wky.stusys.entity.Teacher;
import com.wky.stusys.query.StuSysQueryInfo;

public interface IAdminService {
	
	public Page<Student> findStudentPage(StuSysQueryInfo info);
	public void addStudent(Student student);
	public Student toUpdateStudent(Integer stuId);
	public void updateStudent(Student student);
	
	public Page<Teacher> findTeacherPage(StuSysQueryInfo info);
	public void addTeacher(Teacher teacher);
	public Teacher toUpdateTeacher(Integer teaId);
	public void updateTeacher(Teacher teacher);
	
	public Page<Course> findCoursePage(StuSysQueryInfo info);
	public void addCourse(Course course);
	public Course toUpdateCourse(Integer couId);
	public void updateCourse(Course course);
	
	public Page<StuCou> findStuCouPage(StuSysQueryInfo info);
	public void addStuCou(StuCou stuCou);
	public StuCou toUpdateStuCou(Integer stuCouId);
	public void updateStuCou(StuCou stuCou);
	
	public void deleteStuCou(StuCou stuCou);
}
