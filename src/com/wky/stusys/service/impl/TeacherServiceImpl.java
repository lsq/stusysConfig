package com.wky.stusys.service.impl;

import com.wky.common.page.Page;
import com.wky.stusys.dao.ITeacherDao;
import com.wky.stusys.entity.StuCou;
import com.wky.stusys.entity.Teacher;
import com.wky.stusys.query.StuSysQueryInfo;
import com.wky.stusys.service.ITeacherService;

public class TeacherServiceImpl implements ITeacherService {
	
	private ITeacherDao teacherDao;

	public void setTeacherDao(ITeacherDao teacherDao) {
		this.teacherDao = teacherDao;
	}

	public Teacher findTeacher(Integer teaId) {
		return teacherDao.findTeacher(teaId);
	}

	public void updateTeacher(Teacher teacher) {
		teacherDao.updateTeacher(teacher);
	}

	public Page<StuCou> findTeacherScorePage(StuSysQueryInfo info) {
		return teacherDao.findTeacherScorePage(info);
	}

	public StuCou findStuCou(Integer stuCouId) {
		return teacherDao.findStuCou(stuCouId);
	}

	public void updateStuCou(StuCou stuCou) {
		teacherDao.updateStuCou(stuCou);
	}


}
