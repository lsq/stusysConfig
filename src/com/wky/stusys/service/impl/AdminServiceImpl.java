package com.wky.stusys.service.impl;

import com.wky.common.page.Page;
import com.wky.stusys.dao.IAdminDao;
import com.wky.stusys.entity.Course;
import com.wky.stusys.entity.StuCou;
import com.wky.stusys.entity.Student;
import com.wky.stusys.entity.Teacher;
import com.wky.stusys.query.StuSysQueryInfo;
import com.wky.stusys.service.IAdminService;

public class AdminServiceImpl implements IAdminService {
	
	private IAdminDao adminDao;

	public void setAdminDao(IAdminDao adminDao) {
		this.adminDao = adminDao;
	}

	public Page<Student> findStudentPage(StuSysQueryInfo info) {
		return adminDao.findStudentPage(info);
	}

	public void addStudent(Student student) {
		adminDao.addStudent(student);
	}

	public Student toUpdateStudent(Integer stuId) {
		return adminDao.toUpdateStudent(stuId);
	}

	public void updateStudent(Student student) {
		adminDao.updateStudent(student);
	}

	public Page<Teacher> findTeacherPage(StuSysQueryInfo info) {
		return adminDao.findTeacherPage(info);
	}

	public void addTeacher(Teacher teacher) {
		adminDao.addTeacher(teacher);
	}

	public Teacher toUpdateTeacher(Integer teaId) {
		return adminDao.toUpdateTeacher(teaId);
	}

	public void updateTeacher(Teacher teacher) {
		adminDao.updateTeacher(teacher);
	}

	public Page<Course> findCoursePage(StuSysQueryInfo info) {
		return adminDao.findCoursePage(info);
	}

	public void addCourse(Course course) {
		adminDao.addCourse(course);
	}

	public Course toUpdateCourse(Integer couId) {
		return adminDao.toUpdateCourse(couId);
	}

	public void updateCourse(Course course) {
		adminDao.updateCourse(course);
	}

	public Page<StuCou> findStuCouPage(StuSysQueryInfo info) {
		return adminDao.findStuCouPage(info);
	}

	public void addStuCou(StuCou stuCou) {
		adminDao.addStuCou(stuCou);
	}

	public StuCou toUpdateStuCou(Integer stuCouId) {
		return adminDao.toUpdateStuCou(stuCouId);
	}

	public void updateStuCou(StuCou stuCou) {
		adminDao.updateStuCou(stuCou);
	}

	public void deleteStuCou(StuCou stuCou) {
		adminDao.deleteStuCou(stuCou);
	}


}
