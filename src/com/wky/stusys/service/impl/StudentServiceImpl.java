package com.wky.stusys.service.impl;

import com.wky.common.page.Page;
import com.wky.stusys.dao.IStudentDao;
import com.wky.stusys.entity.StuCou;
import com.wky.stusys.entity.Student;
import com.wky.stusys.query.StuSysQueryInfo;
import com.wky.stusys.service.IStudentService;

public class StudentServiceImpl implements IStudentService {

	private IStudentDao studentDao;

	public void updateStudent(Student student) {
		studentDao.updateStudent(student);
	}
	
	public Student findStudent(StuSysQueryInfo info){
		return studentDao.findStudent(info);
	}

	public Page<StuCou> findStudentScorePage(StuSysQueryInfo info) {
		return studentDao.findStudentScorePage(info);
	}

	public void setStudentDao(IStudentDao studentDao) {
		this.studentDao = studentDao;
	}

}
