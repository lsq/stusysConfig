package com.wky.stusys.service;

import com.wky.common.page.Page;
import com.wky.stusys.entity.StuCou;
import com.wky.stusys.entity.Student;
import com.wky.stusys.query.StuSysQueryInfo;

public interface IStudentService {
	
	Student findStudent(StuSysQueryInfo info);

	void updateStudent(Student student);
	
	Page<StuCou> findStudentScorePage(StuSysQueryInfo info);
}
