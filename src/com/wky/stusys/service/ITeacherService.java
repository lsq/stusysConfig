package com.wky.stusys.service;

import com.wky.common.page.Page;
import com.wky.stusys.entity.StuCou;
import com.wky.stusys.entity.Teacher;
import com.wky.stusys.query.StuSysQueryInfo;

public interface ITeacherService {
	
	Teacher findTeacher(Integer teaId);

	void updateTeacher(Teacher teacher);
	
	Page<StuCou> findTeacherScorePage(StuSysQueryInfo info);

	StuCou findStuCou(Integer stuCouId);

	void updateStuCou(StuCou stuCou);
}
