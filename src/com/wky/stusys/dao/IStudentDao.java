package com.wky.stusys.dao;

import com.wky.common.page.Page;
import com.wky.stusys.entity.StuCou;
import com.wky.stusys.entity.Student;
import com.wky.stusys.query.StuSysQueryInfo;

public interface IStudentDao{
	
	Student findStudent(StuSysQueryInfo info);

	void updateStudent(Student student);
	
	Page<StuCou> findStudentScorePage(StuSysQueryInfo info);
}
