package com.wky.stusys.dao.impl;

import java.util.List;

import com.wky.common.dao.impl.CommonDaoImpl;
import com.wky.common.page.Page;
import com.wky.common.query.QueryInfo;
import com.wky.common.util.Tools;
import com.wky.stusys.dao.IAdminDao;
import com.wky.stusys.entity.Course;
import com.wky.stusys.entity.StuCou;
import com.wky.stusys.entity.Student;
import com.wky.stusys.entity.Teacher;
import com.wky.stusys.query.StuSysQueryInfo;

public class AdminDaoImpl extends CommonDaoImpl implements IAdminDao {

	private Page<?> getMyPage(QueryInfo info, String hql) {
		if (info.getNotPage() != null && info.getNotPage().booleanValue()) {
			List<?> data = this.executeSql(hql);
			return this.putDataToPage(data);
		} else {
			return this.find(hql, info.getPageNumber().intValue(), info
					.getPageSize().intValue());
		}
	}

	private Page<?> getMyPage(QueryInfo info, String hql, Object[] condition) {
		if (info.getNotPage() != null && info.getNotPage().booleanValue()) {
			List<?> data = this.executeSql(hql, condition);
			return this.putDataToPage(data);
		} else {
			return this.find(hql, condition, info.getPageNumber().intValue(),
					info.getPageSize().intValue());
		}
	}
	
	@SuppressWarnings("unchecked")
	public Page<Student> findStudentPage(StuSysQueryInfo info) {
		Object[] values = new Object[10];
		int idx = 0;
		
		StringBuffer hql = new StringBuffer();
		hql.append("from ").append(Student.class.getName()).append(" as s where 1=1 ");
		
		if(Tools.isNotEmptyString(info.getStuName())){
			hql.append(" and s.stuName like ? ");
			values[idx++] = "%" + info.getStuName() + "%";
		}

		hql.append(" order by s.stuId asc");
		
		if (idx > 0) {
			Object[] condition = new Object[idx];
			System.arraycopy(values, 0, condition, 0, idx);
			return (Page<Student>) this.getMyPage(info, hql.toString(), condition);
		}
		return (Page<Student>) this.getMyPage(info, hql.toString());
	}

	public void addStudent(Student student) {
		this.getHibernateTemplate().saveOrUpdate(student);
		this.getHibernateTemplate().flush();
	}

	public Student toUpdateStudent(Integer stuId) {
		return this.getHibernateTemplate().load(Student.class, stuId);
	}

	public void updateStudent(Student student) {
		this.addStudent(student);
	}

	@SuppressWarnings("unchecked")
	public Page<Teacher> findTeacherPage(StuSysQueryInfo info) {
		Object[] values = new Object[10];
		int idx = 0;
		
		StringBuffer hql = new StringBuffer();
		hql.append("from ").append(Teacher.class.getName()).append(" as t where 1=1 ");
		
		if(Tools.isNotEmptyString(info.getTeaName())){
			hql.append(" and t.teaName like ? ");
			values[idx++] = "%" + info.getTeaName() + "%";
		}

		hql.append(" order by t.teaId asc");
		
		if (idx > 0) {
			Object[] condition = new Object[idx];
			System.arraycopy(values, 0, condition, 0, idx);
			return (Page<Teacher>) this.getMyPage(info, hql.toString(), condition);
		}
		return (Page<Teacher>) this.getMyPage(info, hql.toString());
	}

	public void addTeacher(Teacher teacher) {
		this.getHibernateTemplate().saveOrUpdate(teacher);
		this.getHibernateTemplate().flush();
	}

	public Teacher toUpdateTeacher(Integer teaId) {
		return this.getHibernateTemplate().load(Teacher.class, teaId);
	}

	public void updateTeacher(Teacher teacher) {
		this.addTeacher(teacher);
	}

	@SuppressWarnings("unchecked")
	public Page<Course> findCoursePage(StuSysQueryInfo info) {
		Object[] values = new Object[10];
		int idx = 0;
		
		StringBuffer hql = new StringBuffer();
		hql.append("from ").append(Course.class.getName()).append(" as c where 1=1 ");
		
		if(Tools.isNotEmptyString(info.getCouName())){
			hql.append(" and c.couName like ? ");
			values[idx++] = "%" + info.getCouName() + "%";
		}

		hql.append(" order by c.couId asc");
		
		if (idx > 0) {
			Object[] condition = new Object[idx];
			System.arraycopy(values, 0, condition, 0, idx);
			return (Page<Course>) this.getMyPage(info, hql.toString(), condition);
		}
		return (Page<Course>) this.getMyPage(info, hql.toString());
	}

	public void addCourse(Course course) {
		this.getHibernateTemplate().saveOrUpdate(course);
		this.getHibernateTemplate().flush();
	}

	public Course toUpdateCourse(Integer couId) {
		return this.getHibernateTemplate().load(Course.class, couId);
	}

	public void updateCourse(Course course) {
		this.addCourse(course);
	}

	@SuppressWarnings("unchecked")
	public Page<StuCou> findStuCouPage(StuSysQueryInfo info) {
		Object[] values = new Object[10];
		int idx = 0;
		
		StringBuffer hql = new StringBuffer();
		hql.append("from ").append(StuCou.class.getName()).append(" as sc where 1=1 ");
		
		if(info.getStuId() != null){
			hql.append(" and sc.stuId = ? ");
			values[idx++] =  info.getStuId() ;
		}
		
		if(info.getTeaId() != null){
			hql.append(" and sc.teaId = ? ");
			values[idx++] =  info.getTeaId() ;
		}
		
		if(info.getTeaId() != null){
			hql.append(" and sc.couId = ? ");
			values[idx++] =  info.getTeaId() ;
		}
		
		if(Tools.isNotEmptyString(info.getCouName())){
			hql.append(" and sc.course.couName like ? ");
			values[idx++] = "%" + info.getCouName() + "%";
		}
		
		if(Tools.isNotEmptyString(info.getTeaName())){
			hql.append(" and sc.teacher.teaName like ? ");
			values[idx++] = "%" + info.getTeaName() + "%";
		}
		
		if(Tools.isNotEmptyString(info.getStuName())){
			hql.append(" and sc.student.stuName like ? ");
			values[idx++] = "%" + info.getStuName() + "%";
		}
		
		if(info.getScoreFront() != null){
			hql.append(" and sc.score >= ? ");
			values[idx++] =  info.getScoreFront() ;
		}
		
		if(info.getScoreBack() != null){
			hql.append(" and sc.score <= ? ");
			values[idx++] =  info.getScoreBack() ;
		}
		
		hql.append(" order by sc.course.couId asc");
		
		if (idx > 0) {
			Object[] condition = new Object[idx];
			System.arraycopy(values, 0, condition, 0, idx);
			return (Page<StuCou>) this.getMyPage(info, hql.toString(), condition);
		}
		return (Page<StuCou>) this.getMyPage(info, hql.toString());
	}

	public void addStuCou(StuCou stuCou) {
		this.getHibernateTemplate().saveOrUpdate(stuCou);
		this.getHibernateTemplate().flush();
	}

	public StuCou toUpdateStuCou(Integer stuCouId) {
		return this.getHibernateTemplate().load(StuCou.class, stuCouId);
	}

	public void updateStuCou(StuCou stuCou) {
		this.addStuCou(stuCou);
	}

	public void deleteStuCou(StuCou stuCou) {
		this.getHibernateTemplate().delete(stuCou);
	}

}
