package com.wky.stusys.dao;

import com.wky.common.page.Page;
import com.wky.stusys.entity.StuCou;
import com.wky.stusys.entity.Teacher;
import com.wky.stusys.query.StuSysQueryInfo;

public interface ITeacherDao{
	void updateTeacher(Teacher Teacher);
	
	Teacher findTeacher(Integer id);

	Page<StuCou> findTeacherScorePage(StuSysQueryInfo info);

	StuCou findStuCou(Integer stuCouId);

	void updateStuCou(StuCou stuCou);
}
